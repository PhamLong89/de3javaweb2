create database De3JavaWeb;
user De3JavaWeb;

create table MatHang(
MaMatHang varchar(10) not null primary key,
TenMatHang varchar(50) not null,
NhomHang varchar(50) not null,
GiaBan int not null,
SoLuong int not null
);

create table KhachHang(
MaKhachHang varchar(10) not null primary key,
TenKhachHang varchar(50) not null,
DiaChi varchar(100),
SoDienThoai varchar(10)
);


create table HoaDon(
MaHoaDon varchar(10) not null primary key,
NgayBan date not null,
MaKhachHang varchar(10),
foreign key (MaKhachHang) references KhachHang(MaKhachHang)
);

create table ChiTietHD(
	MaChiTietHD int not null primary key AUTO_INCREMENT,
	MaHoaDon varchar(10),
	foreign key (MaHoaDon) references HoaDon(MaHoaDon) on delete cascade,
	MaMatHang varchar(10),
	foreign key (MaMatHang) references MatHang(MaMatHang),
	GiaBan int not null,
	SoLuong int not null
);

DELIMITER &&
DROP FUNCTION IF EXISTS tinh_tong_tien;
CREATE FUNCTION tinh_tong_tien (MaHD_input varchar(10))
RETURNS INTEGER
BEGIN
	DECLARE tong_tien INT;
	SELECT SUM(cthd.GiaBan * cthd.SoLuong)
	FROM ChiTietHD cthd
	WHERE cthd.MaHoaDon = MaHD_input
	INTO tong_tien;
RETURN tong_tien;
END;
&&
DELIMITER ;

create view ViewHoaDon as
	select hd.MaHoaDon, hd.NgayBan, hd.MaKhachHang, kh.TenKhachHang, kh.DiaChi, kh.SoDienThoai, tinh_tong_tien(hd.MaHoaDon) as TongTien from HoaDon hd
	inner join KhachHang kh on hd.MaKhachHang = kh.MaKhachHang;


DROP TRIGGER IF EXISTS deleteHoaDon;
DELIMITER &&
create trigger deleteHoaDon before delete
	on HoaDon
	for each row
	begin
	delete from ChiTietHD where MaHoaDon = old.MaHoaDon;
	END;
	&&
DELIMITER ;

DROP TRIGGER IF EXISTS updateSoLuongMatHang;
DELIMITER &&
create trigger updateSoLuongMatHang after insert
	on ChiTietHD
	for each row
	begin
		update MatHang set SoLuong = SoLuong - New.SoLuong where MaMatHang = new.MaMatHang;
	END;
	&&
DELIMITER ;

DELIMITER &&
DROP PROCEDURE IF EXISTS chi_tiet_HD;
CREATE PROCEDURE chi_tiet_HD (MaHD_input varchar(10))
BEGIN
	select cthd.MaChiTietHD, cthd.MaHoaDon, cthd.MaMatHang, mh.TenMatHang, cthd.GiaBan, cthd.SoLuong, cthd.GiaBan * cthd.SoLuong as ThanhTien from ChiTietHD cthd
	inner join MatHang mh on mh.MaMatHang = cthd.MaMatHang
	where cthd.MaHoaDon = MaHD_input;
END;
&&
DELIMITER ;